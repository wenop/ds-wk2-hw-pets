import AnimalGeneric.PetTrick;

import javax.xml.bind.ValidationException;

public class BirdThatSings extends Bird implements PetTrick {
    public BirdThatSings(String strName) throws ValidationException {
        super(strName);
    }

    @Override
    public String Trick() {
        return "singing!";
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("");
        buffer.append(this.getSpecies()
                + "\t| "    + this.getName()
                + "\t| "    + this.VetAppointment()
                + "\t| "    + this.Trick()
                + "\t| "    + this.Say());
        return buffer.toString();
    }
}
