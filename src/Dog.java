import AnimalGeneric.Pet;

import javax.xml.bind.ValidationException;

public class Dog extends Pet {

    private static final float VET_COST = 1.25f;

    Dog() {
        setSpecies(Species.DOG);
        setVetAppointmentCost(Dog.VET_COST);
    }

    public Dog(String strName) throws ValidationException {
        this();
        setName(strName);
    }

    @Override
    public String Say() {
        // Todo
        return "WOOF!";
    }

    @Override
    public float VetAppointment() {
        return getVetAppointmentCost() * VET_MULTIPLIER;
    }

}
