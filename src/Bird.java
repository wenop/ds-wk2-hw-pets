import AnimalGeneric.Pet;

import javax.xml.bind.ValidationException;

public class Bird extends Pet {

    private static final float VET_COST = .75f;

    Bird() {
        super();
        setSpecies(Species.BIRD);
        setVetAppointmentCost(Bird.VET_COST);
    }

    public Bird(String strName) throws ValidationException {
        this();
        setName(strName);
    }

    @Override
    public String Say() {
        // Todo what's the sound of a bird?
        return "JiJi..";
    }

    @Override
    public float VetAppointment() {
        return getVetAppointmentCost() * VET_MULTIPLIER;
    }

}
