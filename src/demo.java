import AnimalGeneric.Pet;
import javax.xml.bind.ValidationException;

public class demo {

    public static void main(String[] args) throws ValidationException {

        DogThatJumps rocky = new DogThatJumps("Rocky");
        CatThatWagsHead lucky = new CatThatWagsHead("Lucy");
        BirdThatSings jake = new BirdThatSings("Jake");
        SnakeThatSmoke smokey  = new SnakeThatSmoke("Smokey");
        //        Snake smokey  = new Snake("Smokey");

        Pet[] pets = new Pet[]{rocky, lucky, jake, smokey};

        System.out.print("Type of Generic.Pet - Generic.Pet's Name - Generic.Pet's Vet Cost - Generic.Pet's trick  - Generic.Pet's says\n");

        for (Pet p: pets) {
            System.out.print(p.toString() + "\n");
        }
    }
}
