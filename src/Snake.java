import AnimalGeneric.Pet;

import javax.xml.bind.ValidationException;

public class Snake extends Pet {

    private static final float VET_COST = 1.5f;

    Snake() {
        setSpecies(Species.OTHER);
        setVetAppointmentCost(Snake.VET_COST);
    }

    public Snake(String strName) throws ValidationException {
        this();
        setName(strName);
    }

    @Override
    public String Say() {
        // Todo
        return "S-s-s-";
    }

    @Override
    public float VetAppointment() {
        return getVetAppointmentCost() * VET_MULTIPLIER;
    }

}
