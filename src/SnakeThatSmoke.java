import AnimalGeneric.PetTrick;

import javax.xml.bind.ValidationException;

public class SnakeThatSmoke extends Snake implements PetTrick {
    public SnakeThatSmoke(String strName) throws ValidationException {
        super(strName);
    }

    @Override
    public String Trick() {
        return "smoke cigarette";
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("");
        buffer.append(this.getSpecies()
                + "\t| "    + this.getName()
                + "\t| "    + this.VetAppointment()
                + "\t| "    + this.Trick()
                + "\t| "    + this.Say());
        return buffer.toString();
    }
}
