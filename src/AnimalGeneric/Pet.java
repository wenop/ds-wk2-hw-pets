package AnimalGeneric;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.xml.bind.ValidationException;

/**
 *  A generic class that specifies some common attributes and actions of pet
 */
public abstract class Pet {

    public enum Species {
        DOG, CAT, BIRD, OTHER
    }

    /**
     * The name of the Generic.Pet
     */
    private String name;

    /**
     * Species of the Generic.Pet
     *      Species is read-only (which has not setter)
     */
    private Species species = Species.OTHER;

    /**
     * The cost efficient of a vet appointment
     */
    private float vetAppointmentCost = 1.5f;

    /**
     * The multiplier of vet appointment
     */
    protected static final int VET_MULTIPLIER = 100;

    /**
     * Returns the name of the pet
     * @return the name string
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the pet
     * @param name   a new name
     */
    public void setName(String name) throws ValidationException {
        if( name == null || name.isEmpty() ) {
            throw new ValidationException("Pet name not valid");
        }
        this.name = name;
    }

    /**
     * Returns the species of the pet
     * @return      a species enum
     */
    public Species getSpecies() {
        return species;
    }


    /**
     * Set the specie of the pet (visible only to )
     * @param species   the species that specifying
     */
    protected void setSpecies(Species species) {
        this.species = species;
    }

    protected float getVetAppointmentCost() {
        return vetAppointmentCost;
    }

    protected void setVetAppointmentCost(float vetAppointmentCost) {
        this.vetAppointmentCost = vetAppointmentCost;
    }

    /**
     * Returns the sound of the pet
     * @return      a sound the pet would make
     */
    public String Say() throws NotImplementedException {
        throw new NotImplementedException();
    }

    /**
     * Returns the cost of a vet appointment
     * @return      the cost for vet a vet appointment
     */
    public abstract float VetAppointment();

    public Pet() {}

    public Pet(String strName) throws ValidationException {
        setName(strName);
    }

    @Override
    public String toString() {
        String buffer = this.getSpecies()
                + "\t| " + this.getName()
                + "\t| " + this.VetAppointment()
                + "\t| " + "(no trick)"
                + "\t| " + this.Say();
        return buffer;
    }

}
