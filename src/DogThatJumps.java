import AnimalGeneric.PetTrick;

import javax.xml.bind.ValidationException;

public class DogThatJumps extends Dog implements PetTrick {
    public DogThatJumps(String strName) throws ValidationException {
        super(strName);
    }

    @Override
    public String Trick() {
        return "jump!";
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("");
        buffer.append(this.getSpecies()
                + "\t| " + this.getName()
                + "\t| " + this.VetAppointment()
                + "\t| " + this.Trick()
                + "\t| " + this.Say());
        return buffer.toString();
    }
}
