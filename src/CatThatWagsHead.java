import AnimalGeneric.PetTrick;

import javax.xml.bind.ValidationException;

public class CatThatWagsHead extends Cat implements PetTrick {
    public CatThatWagsHead(String strName) throws ValidationException {
        super(strName);
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("");
        buffer.append(this.getSpecies()
                + "\t| "    + this.getName()
                + "\t| "    + this.VetAppointment()
                + "\t| "    + this.Trick()
                + "\t| "    + this.Say());
        return buffer.toString();
    }

    @Override
    public String Trick() {
        return "wag head";
    }
}
