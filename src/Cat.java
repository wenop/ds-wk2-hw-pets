import AnimalGeneric.Pet;

import javax.xml.bind.ValidationException;

public class Cat extends Pet {

    private static final float VET_COST = 1f;

    Cat() {
        super();
        setSpecies(Species.CAT);
        setVetAppointmentCost(Cat.VET_COST);
    }

    public Cat(String strName) throws ValidationException {
        this();
        setName(strName);
    }

    @Override
    public String Say() {
        // Todo
        return "MEW~";
    }

    @Override
    public float VetAppointment() {
        return getVetAppointmentCost() * VET_MULTIPLIER;
    }



}
