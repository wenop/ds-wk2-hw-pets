## Group Member:

 - Guangxue Wen

 - Tongka

 - Bocong Zhao


## Feature 

 1. User input will be validated

        e.g. Pet's name can be set by calling `Pet.setName()`, the setter method will validate input value 
                and throw exception when the `name` string is not valid (say empty string or null)




## Sample output of `demo.java`:

```
Type of Generic.Pet - Generic.Pet's Name - Generic.Pet's Vet Cost - Generic.Pet's trick  - Generic.Pet's says
DOG     	| Rocky     | 125.0	| jump!  	       | WOOF!
CAT	        | Lucy	    | 100.0	| wag head	       | MEW~
BIRD	    | Jake    	| 75.0	| singing!	       | JiJi..
OTHER	    | Smokey	| 150.0	| smoke cigarette  | S-s-s-
```


## UML of the project (partial)

```
             +---------------+
             |      Pet      |
             +----+--+--+----+
                  ^  ^  ^
      +-----------+  |  +------+
      |              |         |
  +---+---+     +----+--+    +-+------+
  |  Dog  |     |  Cat  |    |  Bird  |
  |       |     |       |    |        |
  +-+--+--+     +-------+    +--------+       +---------------+
    ^  ^                                      | <<interface>> |
    |  |                                      |   PetTrick    |
    |  +-----------------+                    +---------------+
    |                    |        <<use>>     |               |
    |  +------------------------------------> |  + Trick()    |
    |  |                 |        <<use>>     |               |
    |  |                 |  +---------------> +---------------+
    |  |                 |  |
+---+--+----------+   +--+--+--------+
| DogThatWagsTail |   | DogThatJumps |      ...
|                 |   |              |   
+-----------------+   +--------------+

```