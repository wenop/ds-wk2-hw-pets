# WK2 HW for Data Structure

##Homework1

_Homework 1: Due: Sept 11 before class_

**How**:  Submit via courseweb 

**What**:  Package labelled Homework1 

Write a java program that includes a pet class

 - the pet's name,

 - the pet's species (Dog, Cat, Bird, or Other)

 - an method method for 'says' different sounds for each animal

 - an abstract VetAppointment method that returns a float cost for pet cost is a static
  variable 100 make a multiplier depending on the type 

 - of animal (1.25 for dog, 1 for cat, .75 for bird, and 1.5 for all other animals)

 - a Dog class, Cat class, Bird class, plus anymore that are necessary extends Generic.Pet class

 - a Generic.PetTrick interface that has one method "Trick" and returns a string saying what the
  pet's trick is (e.g., an animal by animal trick, not species by species).


In a demo class: 
    
  - an array with a Dog, a Cat, a Bird and a Snake stored in the array.

  - print out a summary of the array:
  
        Type of Generic.Pet - Generic.Pet's Name - Generic.Pet's Vet Cost - Generic.Pet's trick  - Generic.Pet's says
